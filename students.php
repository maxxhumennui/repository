<?php

include_once ('StudentsClass.php');
//$db = new DB();
//$link = $db->db_connect();
//$query ="SELECT * FROM `students`";

$students = new StudentsClass();

$result = $students->getStudents();
//var_dump($result);

if($result)
{

    echo "<h3>Students</h3>";
    foreach($result as $value){
        $output = $value[1] . ' ' . $value[2];
        echo '<ol>';
        printf('<a href="assess.php?id=%s">%s</a>',$value[0],$output);
        echo "</ol>";
    }
    echo "";

}

session_start();

if($_SESSION){
    echo "Welcome!". '<br>';
}else{
    header("Location: index.php");
}

echo '<a href="studentFields.php">Add student</a>';

require 'logout.php';